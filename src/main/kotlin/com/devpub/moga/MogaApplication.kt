package com.devpub.moga

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.jpa.repository.config.EnableJpaAuditing

@EnableJpaAuditing
@SpringBootApplication
class MogaApplication

fun main(args: Array<String>) {
	runApplication<MogaApplication>(*args)
}
