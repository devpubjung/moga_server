package com.devpub.moga.api.controller

import com.devpub.moga.API
import com.devpub.moga.VERSION_1
import com.devpub.moga.api.model.entity.AccountItemEntity
import com.devpub.moga.api.model.entity.MoimEntity
import com.devpub.moga.api.model.entity.UserEntity
import com.devpub.moga.model.ApiDataResponse
import com.devpub.moga.model.CommonResponse
import com.devpub.moga.service.AccountItemService
import com.devpub.moga.service.UserService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import lombok.RequiredArgsConstructor
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

@Api(value = "거래내역정보", tags = ["거래내역정보"], description = "거래내역정보를 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("$API$VERSION_1/item")
class AccountItemController : BaseController() {

    @Autowired
    lateinit var accountItemService: AccountItemService

    @ApiOperation(value = "거래내역정보를 저장한다.", notes = "거래내역정보를 저장 한다.")
    @PostMapping("/save")
    fun saveItem(@RequestBody accountItemEntity: AccountItemEntity): CommonResponse {
        accountItemService.saveItem(accountItemEntity)
        return responseService.successResult
    }

    @ApiOperation(value = "거래내역정보 리스트를 저장한다.", notes = "거래내역정보 리스트를 저장 한다.")
    @PostMapping("/saveList")
    fun saveItems(@RequestBody accountItemEntityList: List<AccountItemEntity>): CommonResponse {
        accountItemService.saveItems(accountItemEntityList)
        return responseService.successResult
    }

    @ApiOperation(value = "거래내역정보를 불러온다.", notes = "거래내역정보를 불러온다.")
    @GetMapping("/{id}")
    fun getAccountItem(@PathVariable id: Long, @RequestParam(value = "moimId") moimId: Long): CommonResponse {
        val item = accountItemService.getItem(moimId, id)
        if (item.isEmpty) {
            return responseService.resultFail(message = "거래내역 정보가 없습니다.")
        } else {
            return responseService.result(item)
        }
    }

    @ApiOperation(value = "거래내역정보를 삭제한다.", notes = "거래내역정보를 삭제 한다.")
    @DeleteMapping("/{id}")
    fun deleteAccountItem(@PathVariable id: Long, @RequestParam(value = "moimId") moimId: Long): CommonResponse {
        accountItemService.deleteAccountItem(id, moimId)
        return responseService.successResult
    }
//    @ApiOperation(value = "기존 사용자 정보를 불러온다.", notes = "기존 사용자 정보를 불러온다.")
//    @GetMapping("/{userId}")
//    fun getUser(@PathVariable userId: Long): CommonResponse {
//        val user = userService.getUser(userId)
//        return if (user.isEmpty) {
//            responseService.resultFail(message = "사용자 정보가 없습니다.")
//        } else {
//            responseService.result(user)
//        }
//    }
}