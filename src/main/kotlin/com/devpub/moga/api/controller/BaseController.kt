package com.devpub.moga.api.controller

import com.devpub.moga.API
import com.devpub.moga.VERSION_1
import com.devpub.moga.model.ApiDataResponse
import com.devpub.moga.model.CommonResponse
import lombok.RequiredArgsConstructor
import com.devpub.moga.service.ResponseService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@Api(value = "Base", tags = ["Base"], description = "ResponseService를 담고 있다.")
@RestController
@RequiredArgsConstructor
@RequestMapping("$API$VERSION_1")
class BaseController {

    @Autowired
    lateinit var responseService: ResponseService
}

