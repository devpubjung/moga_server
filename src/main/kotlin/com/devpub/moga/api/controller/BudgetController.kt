package com.devpub.moga.api.controller

import com.devpub.moga.API
import com.devpub.moga.VERSION_1
import com.devpub.moga.api.model.entity.BudgetEntity
import com.devpub.moga.api.model.entity.BudgetSettingRequest
import com.devpub.moga.model.CommonResponse
import com.devpub.moga.service.BudgetService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import lombok.RequiredArgsConstructor
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@Api(value = "예산정보", tags = ["예산정보"], description = "예산정보를 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("$API$VERSION_1/budget")
class BudgetController: BaseController() {

    @Autowired
    lateinit var budgetService: BudgetService

    @ApiOperation(value = "예산정보를 저장한다.", notes = "예산정보를 저장 한다.")
    @PostMapping("/save")
    fun saveCategory(@RequestBody budgetEntity: BudgetEntity): CommonResponse {
        return responseService.result(budgetService.saveBudget(budgetEntity))
    }

    @ApiOperation(value = "예산 리스트정보를 저장한다.", notes = "예산 리스트 정보를 저장 한다.")
    @PostMapping("/saveList")
    fun saveCategoryList(@RequestBody budgetSettingRequest: BudgetSettingRequest): CommonResponse {
        return responseService.result(budgetService.saveBudgetList(budgetSettingRequest))
    }
}