package com.devpub.moga.api.controller

import com.devpub.moga.API
import com.devpub.moga.VERSION_1
import com.devpub.moga.api.model.entity.CategoryEntity
import com.devpub.moga.api.model.entity.MoimEntity
import com.devpub.moga.api.model.entity.UserEntity
import com.devpub.moga.model.ApiDataResponse
import com.devpub.moga.model.CommonResponse
import com.devpub.moga.service.AccountItemService
import com.devpub.moga.service.CategoryService
import com.devpub.moga.service.UserService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import lombok.RequiredArgsConstructor
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.jpa.repository.Query
import org.springframework.web.bind.annotation.*

@Api(value = "카테고리정보", tags = ["카테고리정보"], description = "카테고리정보를 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("$API$VERSION_1/category")
class CategoryController : BaseController() {

    @Autowired
    lateinit var categoryService: CategoryService

    @ApiOperation(value = "카테고리정보를 저장한다.", notes = "카테고리정보를 저장 한다.")
    @PostMapping("/save")
    fun saveCategory(@RequestBody categoryEntity: CategoryEntity): CommonResponse {
        return responseService.result(categoryService.saveCategory(categoryEntity))
    }

    @ApiOperation(value = "카테고리 리스트정보를 저장한다.", notes = "카테고리 리스트 정보를 저장 한다.")
    @PostMapping("/saveList")
    fun saveCategoryList(@RequestBody categoryEntityList: List<CategoryEntity>): CommonResponse {
        return responseService.result(categoryService.saveCategoryList(categoryEntityList))
    }

    @ApiOperation(value = "카테고리정보를 삭제한다.", notes = "카테고리정보를 삭제 한다.")
    @DeleteMapping("/{categoryId}")
    fun deleteCategory(@PathVariable categoryId: Long, @RequestParam(value = "moimId") moimId: Long): CommonResponse {
        categoryService.removeCategory(categoryId, moimId)
        return responseService.successResult
    }
}