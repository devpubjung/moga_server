package com.devpub.moga.api.controller

import com.devpub.moga.API
import com.devpub.moga.VERSION_1
import com.devpub.moga.api.model.dto.KakaoAddress
import com.devpub.moga.api.model.dto.NotiRequest
import com.devpub.moga.api.model.dto.SmsRequest
import com.devpub.moga.api.model.entity.CategoryEntity
import com.devpub.moga.api.model.entity.SuggestEntity
import com.devpub.moga.model.ApiDataResponse
import com.devpub.moga.model.CommonResponse
import com.devpub.moga.service.CategoryService
import com.devpub.moga.service.ParseService
import com.devpub.moga.service.SuggestService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import lombok.RequiredArgsConstructor
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import com.devpub.moga.helper.KakaoRestApiHelper
import com.fasterxml.jackson.databind.ObjectMapper


@Api(value = "기타 기능", tags = ["기타 기능"], description = "기타 기능을 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("$API$VERSION_1/etc")
class EtcController: BaseController() {

    @Autowired
    lateinit var categoryService: CategoryService

    @Autowired
    lateinit var suggestService: SuggestService

    @Autowired
    lateinit var parseService: ParseService

    @Autowired
    lateinit var kakaoRestApiHelper: KakaoRestApiHelper

    @ApiOperation(value = "초기 구동", notes = "초기 필요한 데이터를 만든다.")
    @GetMapping("/init")
    fun init(): ApiDataResponse<*> {
        categoryService.initCategories()
        return responseService.result("success")
    }

    @ApiOperation(value = "기능 제안을 저장한다.", notes = "기능 제안을 저장한다.")
    @PostMapping("/suggest")
    fun saveUser(@RequestBody suggestEntity: SuggestEntity): CommonResponse {
        return responseService.result(suggestService.saveSuggest(suggestEntity))
    }

    @ApiOperation(value = "문자 내용을 파싱한다.", notes = "문자 내용을 파싱한다.")
    @PostMapping("/parsing/sms")
    fun parseSms(@RequestBody smsRequest: SmsRequest): CommonResponse {
        return responseService.result(parseService.parseSmsMessage(smsRequest.moimId, smsRequest.content, smsRequest.date))
    }

    @ApiOperation(value = "푸시 내용을 파싱한다.", notes = "푸시 내용을 파싱한다.")
    @PostMapping("/parsing/noti")
    fun parseNoti(@RequestBody notiRequest: NotiRequest): CommonResponse {
        return responseService.result(parseService.parseNotification(notiRequest))
    }

    @ApiOperation(value = "카카오 스토어 정보를 가져온다.", notes = "카카오 스토어 정보를 가져온다.")
    @GetMapping("/store")
    fun getStoreInfoList(@RequestParam(value = "name") name: String?): CommonResponse {
        if (name.isNullOrEmpty()) {
            return responseService.failResult
        }
        val result = kakaoRestApiHelper.getSearchPlaceByKeyword(name).toString()
        val kakaoAddress = ObjectMapper().readTree(result)
        return responseService.result(kakaoAddress)
    }

    @ApiOperation(value = "스토어 정보를 가지고 카테고리를 가져온다.", notes = "스토어 정보를 가지고 카테고리를 가져온다.")
    @GetMapping("/store/category")
    fun getStoreCategory(@RequestParam(value = "name") name: String?): CommonResponse {
        if (name.isNullOrEmpty()) {
            return responseService.failResult
        }
        return responseService.result(kakaoRestApiHelper.getSearchPlaceFirstCategory(name))
    }
}