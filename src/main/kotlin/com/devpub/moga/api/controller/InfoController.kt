package com.devpub.moga.api.controller

import com.devpub.moga.API
import com.devpub.moga.VERSION_1
import com.devpub.moga.model.ApiDataResponse
import com.devpub.moga.model.CommonResponse
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import lombok.RequiredArgsConstructor
import org.springframework.web.bind.annotation.*

@Api(value = "서비스정보", tags = ["서비스정보"], description = "사용자 정보를 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("$API$VERSION_1")
class InfoController: BaseController() {

    @ApiOperation(value = "ping test", notes = "ping test")
    @GetMapping("/ping")
    fun ping(): CommonResponse {
        return responseService.successResult
    }

    @ApiOperation(value = "서비스 정보", notes = "서비스 정보를 조회 한다.")
    @GetMapping("/info")
    fun getInfo(): ApiDataResponse<*> {
        return responseService.result("AuthResponse(token, member)")
    }
}

