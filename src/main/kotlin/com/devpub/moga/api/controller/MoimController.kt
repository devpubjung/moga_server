package com.devpub.moga.api.controller

import com.devpub.moga.API
import com.devpub.moga.VERSION_1
import com.devpub.moga.api.model.JoinedMoim
import com.devpub.moga.api.model.dto.MoimRequest
import com.devpub.moga.api.model.dto.UpdatedMoimData
import com.devpub.moga.model.CommonResponse
import com.devpub.moga.service.AccountItemService
import com.devpub.moga.service.MoimService
import com.devpub.moga.service.UserService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import lombok.RequiredArgsConstructor
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.lang.StringBuilder
import java.util.*

@Api(value = "모임정보", tags = ["모임정보"], description = "모임 정보를 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("$API$VERSION_1/moim")
class MoimController : BaseController() {

    @Autowired
    lateinit var moimService: MoimService

    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var accountItemService: AccountItemService

    @ApiOperation(value = "모임정보를 저장한다.", notes = "모임정보를 저장 한다.")
    @PostMapping("/save")
    fun saveMoim(@RequestBody moimRequest: MoimRequest): CommonResponse {
        moimService.saveMoim(moimRequest)
        return responseService.successResult
    }

    @ApiOperation(value = "모임정보를 불러온다.", notes = "모임정보를 불러온다.")
    @GetMapping("/{moimId}")
    fun getMoim(@PathVariable moimId: Long): CommonResponse {
        val result = moimService.getMoim(moimId)
        return if (result.isEmpty) {
            responseService.resultFail(message = "모임 정보가 없습니다.")
        } else {
            responseService.result(result)
        }
    }

    @ApiOperation(value = "모임정보를 불러온다.", notes = "모임정보를 불러온다.")
    @GetMapping("/update/{id}")
    fun getUpdateMoimData(@PathVariable id: Long, @RequestParam(value = "date") date: String?): CommonResponse {
        val moimDto = moimService.getMoim(id)
        return if (!moimDto.isEmpty) {
            val accountItems = accountItemService.getActiveItems(id, date)
            val nonAccountItems = accountItemService.getNonActiveItems(id, date)
            responseService.result(UpdatedMoimData(moimDto.get(), accountItems, nonAccountItems))
        } else {
            responseService.resultFail(message = "모임 정보가 없습니다.")
        }
    }

    @ApiOperation(value = "모임에 참여한다.", notes = "모임에 참여한다.")
    @PostMapping("/join")
    fun joinMoim(@RequestBody joinedMoim: JoinedMoim): CommonResponse {
        try {
            val userId = String(Base64.getDecoder().decode(joinedMoim.encodedUserId)).toLong()
            val moimJoinCode = String(Base64.getDecoder().decode(joinedMoim.encodedMoimCode)).toLong()

            val userEntity = userService.getUser(userId)
            val moimEntity = moimService.getMoimEntity(moimJoinCode)

            if (!userEntity.isEmpty && !moimEntity.isEmpty) {
                try {
                    joinedMoim.encodedMoimId?.let {
                        val moimId = String(Base64.getDecoder().decode(it)).toLong()

                        if (moimId == moimJoinCode) {
                            return responseService.resultFail(message = "현재 참여중인 모임입니다.")
                        }

                        val curMoimEntity = moimService.getMoimEntity(moimId)
                        if (!curMoimEntity.isEmpty) {
                            val curMoimUsers = curMoimEntity.get().userList.split(",")

                            val sb = StringBuilder()
                            curMoimUsers.forEachIndexed { index, id ->
                                if (userId != id.toLong()) {
                                    sb.append(id)
                                    if (index > 0) {
                                        sb.append(",")
                                    }
                                }
                            }
                            if (sb.isNotEmpty()) {
                                val curMoim = curMoimEntity.get().copy(userList = sb.toString())
                                moimService.updateMoim(curMoim)
                            } else {
                                // 모임 삭제
                                moimService.removeMoim(curMoimEntity.get())
                            }
                        }
                    }
                } catch (e: Exception) {
                }

                moimEntity.get().run {
                    moimService.updateMoim(copy(userList = "$userList,$userId"))
                }
                return responseService.result(moimService.getMoim(moimJoinCode))
            }
            return responseService.resultFail(message = "존재하지 않는 모임입니다.")
        } catch (e: Exception) {
            return responseService.resultFail(message = "올바르지 않은 코드를 입력하셨습니다.")
        }
    }
}