package com.devpub.moga.api.controller

import com.devpub.moga.API
import com.devpub.moga.VERSION_1
import com.devpub.moga.api.model.entity.MoimEntity
import com.devpub.moga.api.model.entity.UserEntity
import com.devpub.moga.model.ApiDataResponse
import com.devpub.moga.model.CommonResponse
import com.devpub.moga.service.UserService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import lombok.RequiredArgsConstructor
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@Api(value = "사용자정보", tags = ["사용자정보"], description = "사용자 정보를 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("$API$VERSION_1/user")
class UserController: BaseController() {

    @Autowired
    lateinit var userService: UserService

    @ApiOperation(value = "사용자 정보를 저장한다.", notes = "사용자 정보를 저장 한다.")
    @PostMapping("/save")
    fun saveUser(@RequestBody userEntity: UserEntity): ApiDataResponse<*> {
        return responseService.result(userService.saveUser(userEntity))
    }

    @ApiOperation(value = "사용자 정보를 업데이트한다.", notes = "사용자 정보를 업데이트 한다.")
    @PutMapping("/update")
    fun updateUser(@RequestBody userEntity: UserEntity): ApiDataResponse<*> {
        return responseService.result(userService.updateUser(userEntity))
    }

    @ApiOperation(value = "기존 사용자 정보를 불러온다.", notes = "기존 사용자 정보를 불러온다.")
    @GetMapping("/{userId}")
    fun getUser(@PathVariable userId: Long): CommonResponse {
        val user = userService.getUser(userId)
        return if (user.isEmpty) {
            responseService.resultFail(message = "사용자 정보가 없습니다.")
        } else {
            responseService.result(user)
        }
    }

    @ApiOperation(value = "탈퇴한 사용자 정보를 불러온다.", notes = "탈퇴한 사용자 정보를 불러온다.")
    @GetMapping("/{userId}/delete")
    fun getUserForDelete(@PathVariable userId: Long): CommonResponse {
        val user = userService.getUserForDelete(userId)
        return if (user.isEmpty) {
            responseService.resultFail(message = "사용자 정보가 없습니다.")
        } else {
            responseService.result(user)
        }
    }
}