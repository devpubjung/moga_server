package com.devpub.moga.api.model

import java.io.Serializable

data class ErrorDto(val errorCode: Int = 0,
                    val errorMessage: String) : Serializable