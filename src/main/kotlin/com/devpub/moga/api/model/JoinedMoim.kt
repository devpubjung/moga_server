package com.devpub.moga.api.model

import java.io.Serializable

data class JoinedMoim(val encodedUserId: String, val encodedMoimId: String? = null, val encodedMoimCode: String) : Serializable