package com.devpub.moga.api.model

import lombok.AllArgsConstructor
import lombok.Getter

@Getter
@AllArgsConstructor
enum class Role(val value: String) {
    USER("ROLE_USER"),
    ADMIN("ROLE_ADMIN");
}

@Getter
@AllArgsConstructor
enum class Provider(val value: String) {
    NONE("none"),
    KAKAO("kakao"),
    NAVER("naver"),
    GOOGLE("google")
}

@Getter
@AllArgsConstructor
enum class Gender(val value: Int) {
    MALE(0),
    FEMALE(1),
}

@Getter
@AllArgsConstructor
enum class Target {
    INCOME,
    EXPEND,
    TRANSFER
}