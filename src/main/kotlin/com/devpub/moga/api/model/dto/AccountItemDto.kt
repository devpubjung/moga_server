package com.devpub.moga.api.model.dto

import com.devpub.moga.api.model.Target
import com.devpub.moga.api.model.entity.CategoryEntity
import com.devpub.moga.api.model.entity.UserEntity
import java.io.Serializable
import java.util.*
import javax.persistence.EnumType
import javax.persistence.Enumerated

data class AccountItemDto(
        val moimId: Long,
        val id: Long,
        @Enumerated(EnumType.STRING)
        val target: Target,
        val date: Date,
        val user: UserEntity,
        val amount: Long = 0,
        val assets: Assets,
        val category: CategoryEntity,
        val content: String? = null,
        val memo: String? = null,
) : Serializable {
}

data class Assets(val moimId: Long, val name: String) : Serializable