package com.devpub.moga.api.model.dto

import com.devpub.moga.api.model.entity.CategoryEntity
import com.fasterxml.jackson.annotation.JsonIgnore
import java.io.Serializable

data class BudgetRequest(val moimId: Long,
                         val categoryId: Long,
                         val budget: Int,
                         val weeklyBudget: Int,
                         val maxBudget: Int) : Serializable {
//    "budget": 100,
//    "categoryId": 1,
//    "maxBudget": 150,
//    "moimId": 115964281461,
//    "weeklyBudget": 20
}

data class BudgetDto(val moimId: Long,
                         val category: CategoryEntity,
                         val budget: Int,
                         val weeklyBudget: Int,
                         val maxBudget: Int) : Serializable {
//    "budget": 100,
//    "categoryId": 1,
//    "maxBudget": 150,
//    "moimId": 115964281461,
//    "weeklyBudget": 20
}