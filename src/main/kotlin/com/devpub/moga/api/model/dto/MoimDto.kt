package com.devpub.moga.api.model.dto

import com.devpub.moga.api.model.entity.BudgetEntity
import com.devpub.moga.api.model.entity.CategoryEntity
import com.devpub.moga.api.model.entity.TimeEntity
import com.devpub.moga.api.model.entity.UserEntity
import java.io.Serializable

class MoimDto : TimeEntity(), Serializable {
    var id: Long = 1
    var ownerId: Long = -1
    val userList: MutableList<UserEntity> = mutableListOf()
    val budgetList: MutableList<BudgetDto> = mutableListOf()
    val categoryList: MutableList<CategoryEntity> = mutableListOf()
}

data class MoimRequest(
        val id: Long,
        val ownerId: Long,
        val userList: List<UserEntity>,
        val budgetList: List<BudgetEntity>,
) : Serializable {
//    {
//        "budgetList": [
//        {
//            "budget": 100,
//            "categoryId": 1,
//            "maxBudget": 150,
//            "moimId": 115964281461,
//            "weeklyBudget": 20
//        }
//        ],
//        "id": 115964281461,
//        "ownerId": 11596428146,
//        "userList": [
//        {
//            "ageRange": 3,
//            "birthYear": 1991,
//            "email": "overcome26@kakao.com",
//            "gender": "MALE",
//            "id": 11596428146,
//            "nickName": "정효찬(henry.jung)",
//            "profileImgUrl": "https://k.kakaocdn.net/dn/czrp1n/btqSsQZAZ3H/cekSkU0QhCKnNr0xOV6hT0/img_640x640.jpg",
//            "provider": "KAKAO",
//            "thumbNailImgUrl": "https://k.kakaocdn.net/dn/czrp1n/btqSsQZAZ3H/cekSkU0QhCKnNr0xOV6hT0/img_110x110.jpg",
//            "uid": 1596428146
//        }
//        ]
//    }
}

data class UpdatedMoimData(
        val moim: MoimDto,
        val accountItemList: List<AccountItemDto>,
        val removedAccountItemList: List<AccountItemDto>,
) : Serializable