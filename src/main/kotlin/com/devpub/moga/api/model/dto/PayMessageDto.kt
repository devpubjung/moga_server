package com.devpub.moga.api.model.dto

import com.devpub.moga.api.model.Target
import com.devpub.moga.api.model.entity.CategoryEntity
import com.fasterxml.jackson.annotation.JsonFormat
import java.io.Serializable
import java.util.*

class PayMessage(var time: Date) : Serializable {
    var target: Target? = null
    var assetsName: String = ""
    var payMoney: Long = 0
    var storeName: String = ""
    var content: String = ""
    var isBank: Boolean = false
    var category: CategoryEntity? = null
}

data class SmsRequest(
        val sender: String,
        val content: String,
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd HH:mm:ss", timezone = "Asia/Seoul")
        val date: Date,
        val moimId: Long = -1
) : Serializable

data class NotiRequest(
        val packagerName: String,
        val title: String,
        val text: String,
        val subText: String?,
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd HH:mm:ss", timezone = "Asia/Seoul")
        val date: Date,
        val moimId: Long = -1,
) : Serializable