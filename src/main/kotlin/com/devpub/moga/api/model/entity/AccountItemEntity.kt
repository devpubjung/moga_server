package com.devpub.moga.api.model.entity

import com.devpub.moga.api.model.Target
import com.fasterxml.jackson.annotation.JsonFormat
import org.hibernate.annotations.Where
import org.springframework.format.annotation.DateTimeFormat
import java.io.Serializable
import java.util.*
import javax.persistence.*


@Entity
@Table(name = "account_item")
@Where(clause="is_active=1")
@IdClass(AccountItemIdClass::class)
data class AccountItemEntity(
        @Id
        val moimId: Long,
        @Id
        val id: Long,
        @Enumerated(EnumType.STRING)
        val target: Target,
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy/MM/dd HH:mm:ss", timezone = "Asia/Seoul")
        val date: Date,
        val userId: Long,
        val amount: Long = 0,
        val assets: String,
        val categoryId: Long,
        val content: String? = null,
        val memo: String? = null,

        @Column(nullable = false, columnDefinition = "tinyint(1) default 1")
        val isActive : Boolean = true
) : TimeEntity(), Serializable {
}

data class AccountItemIdClass(
        val moimId: Long = -1,
        val id: Long = -1,
) : Serializable