package com.devpub.moga.api.model.entity

import org.hibernate.annotations.Where
import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "budget")
@Where(clause="is_active=1")
@IdClass(BudgetIdClass::class)
data class BudgetEntity(
        @Id
        @Column(name = "moim_id")
        val moimId: Long = -1,
        @Id
        val categoryId: Long = -1,
        val budget: Int,
        val weeklyBudget: Int,
        val maxBudget: Int,

        @Column(nullable = false, columnDefinition = "tinyint(1) default 1")
        val isActive : Boolean = true
) : TimeEntity(), Serializable


data class BudgetIdClass(
        val moimId: Long = -1,
        val categoryId: Long = -1,
) : Serializable


data class BudgetSettingRequest(
        val budgetList: List<BudgetEntity>,
        val removeBudgetList: List<BudgetEntity>,
) : Serializable