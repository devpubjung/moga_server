package com.devpub.moga.api.model.entity

import com.devpub.moga.api.model.Target
import org.hibernate.annotations.Where
import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "category")
@Where(clause="is_active=1")
data class CategoryEntity(
        @Column(nullable = false, unique = true)
        val name: String = "전체",
        @Id
        val id: Long = -1,
        val kosisId: Long = -1,
        @Enumerated(EnumType.STRING)
        val target: Target,
        val tag: String = name.replace("/", ","),
        @Column(name = "moim_id", nullable = true)
        val moimId: Long? = -1,

        @Column(nullable = true)
        val ownerId: Long? = null, //userId

        val parentId: Long = id,

        @Column(nullable = false, columnDefinition = "tinyint(1) default 1")
        val isActive : Boolean = true
) : TimeEntity(), Serializable

data class CategoryIdClass(
        val id: Long = -1,
        val name: String = "전체",
) : Serializable