package com.devpub.moga.api.model.entity

import org.hibernate.annotations.Where
import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "moim")
@Where(clause="is_active=1")
data class MoimEntity(
        @Id
        val id: Long = -1, //userId+No

        val ownerId: Long = -1, //userId

        val userList: String = "", // userId, userId, ...

        @Column(nullable = false, columnDefinition = "tinyint(1) default 1")
        val isActive : Boolean = true
) : TimeEntity(), Serializable
