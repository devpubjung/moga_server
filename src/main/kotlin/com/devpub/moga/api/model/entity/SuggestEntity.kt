package com.devpub.moga.api.model.entity

import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "suggest")
data class SuggestEntity(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id: Long = -1,

        @Column(nullable = false)
        val userId: Long,

        @Column(nullable = false)
        val content: String

) : TimeEntity(), Serializable