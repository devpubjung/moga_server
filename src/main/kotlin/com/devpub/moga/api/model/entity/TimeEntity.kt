package com.devpub.moga.api.model.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import lombok.Getter
import org.hibernate.annotations.CreationTimestamp
import org.hibernate.annotations.UpdateTimestamp
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.EntityListeners
import javax.persistence.MappedSuperclass
import javax.persistence.PreUpdate

import javax.persistence.PrePersist

@Getter
@MappedSuperclass
@EntityListeners(AuditingEntityListener::class)
open class TimeEntity {
    @JsonIgnore
    @CreationTimestamp
    @Column(updatable = false)
    var createdDate: LocalDateTime? = null

    @JsonIgnore
    @UpdateTimestamp
    var updatedDate: LocalDateTime? = null

}