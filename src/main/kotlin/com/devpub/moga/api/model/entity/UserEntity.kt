package com.devpub.moga.api.model.entity

import com.devpub.moga.api.model.Gender
import com.devpub.moga.api.model.Provider
import org.hibernate.annotations.Where
import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "user")
@Where(clause="is_active=1")
data class UserEntity(
        @Id
        val id: Long = -1,
        @Column(nullable = false, unique = true)
        val uid: Long = -1,
        @Enumerated(EnumType.STRING)
        val provider: Provider = Provider.KAKAO,
        val nickName: String= "",
        val email: String = "",
        val profileImgUrl: String = "",
        val thumbNailImgUrl: String = "",
        val ageRange: Int = 1,
        val birthYear: Int = 1987,
        @Enumerated(EnumType.STRING)
        val gender: Gender = Gender.MALE,

        @Column(nullable = false, columnDefinition = "tinyint(1) default 1")
        val isActive : Boolean = true
) : TimeEntity(), Serializable