package com.devpub.moga.api.model.vo;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class SearchVO {

    String id;
    int currentPage = 1;
    int pageSize = 10;

    int userPkid;
    Date regDate;
    String convertRegDate;

    String keyword;
    String keywordNm;
    int keywordCnt;

    String searchType;

}
