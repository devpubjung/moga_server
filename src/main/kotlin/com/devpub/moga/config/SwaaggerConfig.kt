package com.devpub.moga.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import springfox.documentation.swagger2.annotations.EnableSwagger2
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.spi.DocumentationType
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.builders.PathSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.service.Contact
import java.util.ArrayList
import java.util.HashSet

@Configuration
@EnableSwagger2
class SwaaggerConfig {
    private var version: String = "v1"
    private var title: String = "모두의가계부 API $version"

    @Bean
    fun apiV1(): Docket {
        version = "v1"
        title = "모두의가계부 API $version"

        return Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false) // 기본으로 세팅되는 200,401,403,404 메시지를 표시 하지 않음
                .groupName(version)
                .consumes(getConsumeContentTypes())
                .produces(getProduceContentTypes())
                .apiInfo(getApiInfo(title, version))
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.devpub.moga"))
                .paths(PathSelectors.any())
                .build()
    }

    private fun getConsumeContentTypes(): Set<String>? {
        val consumes: MutableSet<String> = HashSet()
        consumes.add("application/json;charset=UTF-8")
        consumes.add("application/x-www-form-urlencoded")
        return consumes
    }

    private fun getProduceContentTypes(): Set<String>? {
        val produces: MutableSet<String> = HashSet()
        produces.add("application/json;charset=UTF-8")
        return produces
    }

    private fun getApiInfo(title: String, version: String): ApiInfo {
        return ApiInfoBuilder()
                .title(title)
                .description("[모두의가계부] API")
                .contact(Contact("데브펍", "https://www.example.com/", "overcome26@gmail.com"))
                .version(version)
                .build()
    }
}