package com.devpub.moga.helper

import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import org.springframework.web.client.RestTemplate
import org.springframework.http.*
import org.springframework.stereotype.Component
import java.net.URI
import java.net.URLEncoder

@Component
class KakaoRestApiHelper {
    private val restApiKey = "391ac63a09365a534a2de4e6eaf8bbbe"

    fun getSearchPlaceByKeyword(name: String): JsonArray {
        val queryString = "?query=" + URLEncoder.encode(name, "UTF-8") + "&page=1&size=15"
        val restTemplate = RestTemplate()
        val headers = HttpHeaders()
        headers.add("Authorization", "KakaoAK $restApiKey")
        headers.add("Accept", MediaType.APPLICATION_JSON_VALUE)
        headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE + ";charset=UTF-8")
        val url = URI.create(API_SERVER_HOST + SEARCH_PLACE_KEYWORD_PATH + queryString)
        val rq = RequestEntity<String>(headers, HttpMethod.GET, url)

        val response = restTemplate.exchange(rq, String::class.java)

        val element: JsonElement = JsonParser().parse(response.body)
        return element.asJsonObject.get("documents") as JsonArray
    }

    fun getSearchPlaceFirstCategory(name: String): String? {
        return getSearchPlaceByKeyword(name).firstOrNull()?.asJsonObject?.getAsJsonPrimitive("category_name")?.asString
    }

    companion object {
        private const val API_SERVER_HOST = "https://dapi.kakao.com"
        private const val SEARCH_PLACE_KEYWORD_PATH = "/v2/local/search/keyword.json"
    }
}