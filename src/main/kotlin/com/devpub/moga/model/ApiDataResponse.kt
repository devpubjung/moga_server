package com.devpub.moga.model

import io.swagger.annotations.ApiModelProperty


class ApiDataResponse<T> : CommonResponse() {

    @ApiModelProperty(value = "응답 DATA")
    var data: T? = null
}