package com.devpub.moga.model

import io.swagger.annotations.ApiModelProperty


open class CommonResponse {

    @ApiModelProperty(value = "응답 성공여부 : true/false")
    var success: Boolean = false

    @ApiModelProperty(value = "응답 코드 번호 : 0>= 정상, < 0 비정상")
    var code: Int = -1

    @ApiModelProperty(value = "응답 메세지")
    var message: String = ""
}