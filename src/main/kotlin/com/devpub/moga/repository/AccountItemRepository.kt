package com.devpub.moga.repository

import com.devpub.moga.api.model.entity.AccountItemEntity
import com.devpub.moga.api.model.entity.AccountItemIdClass
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

@Repository
interface AccountItemRepository : JpaRepository<AccountItemEntity, AccountItemIdClass> {

    @Query("SELECT * FROM moga.account_item WHERE moim_id = ?1 and is_active = 1 order by date", nativeQuery = true)
    fun getActiveAccountItem(moimId: Long): List<AccountItemEntity>

    @Query("SELECT * FROM moga.account_item WHERE moim_id = ?1 and is_active = 0 order by date", nativeQuery = true)
    fun getNonActiveAccountItem(moimId: Long): List<AccountItemEntity>

    @Query(
        "SELECT * FROM moga.account_item WHERE moim_id = ?1 and updated_date >= ?2 and is_active = 1 order by date",
        nativeQuery = true
    )
    fun getActiveAccountItemFromDate(moimId: Long, date: String): List<AccountItemEntity>

    @Query(
        "SELECT * FROM moga.account_item WHERE moim_id = ?1 and updated_date >= ?2 and is_active = 0 order by date",
        nativeQuery = true
    )
    fun getNonActiveAccountItemFromDate(moimId: Long, date: String): List<AccountItemEntity>

    @Modifying
    @Transactional
    @Query("UPDATE moga.account_item SET category_id = ?3 WHERE moim_id = ?1 and category_id = ?2", nativeQuery = true)
    fun updateAccountItemCategory(moimId: Long, removeCategoryId: Long, parentCategoryId: Long)

    @Modifying
    @Transactional
    @Query("UPDATE moga.account_item SET is_active = 1 WHERE id = ?1 and moim_id = ?2", nativeQuery = true)
    fun activeAccountItem(id: Long, moimId: Long)

    @Modifying
    @Transactional
    @Query("UPDATE moga.account_item SET is_active = 0 WHERE id = ?1 and moim_id = ?2", nativeQuery = true)
    fun removeAccountItem(id: Long, moimId: Long)

    @Modifying
    @Transactional
    @Query("UPDATE moga.account_item SET is_active = 0 WHERE moim_id = ?1", nativeQuery = true)
    fun removeAccountItemAll(moimId: Long)


}