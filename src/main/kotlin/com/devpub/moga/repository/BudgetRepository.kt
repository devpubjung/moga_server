package com.devpub.moga.repository

import com.devpub.moga.api.model.entity.BudgetEntity
import com.devpub.moga.api.model.entity.BudgetIdClass
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

@Repository
interface BudgetRepository : JpaRepository<BudgetEntity, BudgetIdClass> {

    @Query("SELECT * FROM moga.budget where moim_id = ?1 and is_active = 1", nativeQuery = true)
    fun getBudgetList(moimId : Long) : List<BudgetEntity>

    @Modifying
    @Transactional
    @Query("UPDATE moga.budget SET is_active = 1 WHERE category_id = ?1 and moim_id = ?2", nativeQuery = true)
    fun activeBudget(categoryId:Long, moimId: Long)

    @Modifying
    @Transactional
    @Query("UPDATE moga.budget SET is_active = 1 WHERE category_id in ?1 and moim_id = ?2", nativeQuery = true)
    fun activeBudgets(categoryIds:List<Long>, moimId: Long)

    @Modifying
    @Transactional
    @Query("UPDATE moga.budget SET is_active = 0 WHERE category_id = ?1 and moim_id = ?2", nativeQuery = true)
    fun removeBudget(categoryId:Long, moimId: Long)

    @Modifying
    @Transactional
    @Query("UPDATE moga.budget SET is_active = 0 WHERE category_id in ?1 and moim_id = ?2", nativeQuery = true)
    fun removeBudgets(categoryIds:List<Long>, moimId: Long)

    @Modifying
    @Transactional
    @Query("UPDATE moga.budget SET is_active = 0 WHERE moim_id = ?1", nativeQuery = true)
    fun removeBudgetAll(moimId: Long)
}