package com.devpub.moga.repository

import com.devpub.moga.api.model.entity.CategoryEntity
import com.devpub.moga.api.model.entity.CategoryIdClass
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.util.*
import javax.transaction.Transactional

@Repository
interface CategoryRepository : JpaRepository<CategoryEntity, CategoryIdClass> {

    @Query("SELECT id, name, ifnull(moim_id, ?2) moim_id, parent_id, kosis_id, target, tag, owner_id, created_date, updated_date, is_active FROM moga.category where id = ?1 and (moim_id = ?2 or moim_id is null) and is_active = 1 order by moim_id limit 1", nativeQuery = true)
    fun getCategory(id: Long, moimId: Long): Optional<CategoryEntity>

    @Query("SELECT id, name, ifnull(moim_id, ?1) moim_id, parent_id, kosis_id, target, tag, owner_id, created_date, updated_date, is_active FROM moga.category where (moim_id = ?1 or moim_id is null) and is_active = 1 order by moim_id", nativeQuery = true)
    fun getCategoryList(moimId: Long): List<CategoryEntity>

    @Modifying
    @Transactional
    @Query("UPDATE moga.category SET is_active = 1 WHERE id = ?1 and moim_id = ?2", nativeQuery = true)
    fun activeCategory(categoryId: Long, moimId: Long)

    @Modifying
    @Transactional
    @Query("UPDATE moga.category SET is_active = 0 WHERE id = ?1 and moim_id = ?2", nativeQuery = true)
    fun removeCategory(categoryId: Long, moimId: Long)

    @Modifying
    @Transactional
    @Query("UPDATE moga.category SET is_active = 0 WHERE moim_id = ?1", nativeQuery = true)
    fun removeCategoryAll(moimId: Long)
}