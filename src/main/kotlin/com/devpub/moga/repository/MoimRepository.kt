package com.devpub.moga.repository

import com.devpub.moga.api.model.entity.MoimEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.util.*
import javax.transaction.Transactional

@Repository
interface MoimRepository : JpaRepository<MoimEntity, Long> {

    @Query("SELECT * FROM moga.category where moim_id = ?1 or moim_id is null order by moim_id", nativeQuery = true)
    fun getMoimForUserId(userId: Long) :Optional<MoimEntity>

    @Modifying
    @Transactional
    @Query("UPDATE moga.moim SET is_active = 1 WHERE id = ?1", nativeQuery = true)
    fun activeMoim(id: Long)

    @Modifying
    @Transactional
    @Query("UPDATE moga.moim SET is_active = 0 WHERE id = ?1", nativeQuery = true)
    fun removeMoim(moimId: Long)
}