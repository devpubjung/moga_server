package com.devpub.moga.repository

import com.devpub.moga.api.model.entity.SuggestEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface SuggestRepository : JpaRepository<SuggestEntity, Long> {

}