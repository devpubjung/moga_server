package com.devpub.moga.repository

import com.devpub.moga.api.model.entity.UserEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface UserRepository : JpaRepository<UserEntity, Long> {

    @Query("SELECT * FROM moga.user where id = ?1 and is_active = 0", nativeQuery = true)
    fun findByIdForNonActive(id: Long): Optional<UserEntity>


    @Query("SELECT * FROM moga.user where id in (?1) and is_active = 1", nativeQuery = true)
    fun getUserList(userIds : String) : List<UserEntity>
}