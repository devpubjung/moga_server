package com.devpub.moga.service

import com.devpub.moga.api.model.dto.AccountItemDto
import com.devpub.moga.api.model.dto.Assets
import com.devpub.moga.api.model.dto.BudgetDto
import com.devpub.moga.api.model.entity.AccountItemEntity
import com.devpub.moga.api.model.entity.AccountItemIdClass
import com.devpub.moga.api.model.entity.UserEntity
import com.devpub.moga.repository.AccountItemRepository
import com.devpub.moga.repository.CategoryRepository
import com.devpub.moga.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class AccountItemService {

    @Autowired
    lateinit var accountItemRepository: AccountItemRepository

    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var categoryRepository: CategoryRepository

    fun saveItem(accountItemEntity: AccountItemEntity): AccountItemEntity {
        accountItemRepository.activeAccountItem(accountItemEntity.id, accountItemEntity.moimId)

        return accountItemRepository.save(accountItemEntity)
    }

    fun saveItems(accountItemEntityList: List<AccountItemEntity>): List<AccountItemEntity> {
        accountItemEntityList.forEach {
            accountItemRepository.activeAccountItem(it.id, it.moimId)
            accountItemRepository.save(it)
        }

        return accountItemEntityList
    }

    fun updateItemCategory(accountItemEntity: AccountItemEntity): AccountItemEntity {
        accountItemRepository.activeAccountItem(accountItemEntity.id, accountItemEntity.moimId)

        return accountItemRepository.save(accountItemEntity)
    }

    fun getItem(moimId: Long, id: Long): Optional<AccountItemDto> {
        val item = accountItemRepository.findById(AccountItemIdClass(moimId, id))
        if (item.isEmpty) {
            return Optional.empty()
        } else {
            item.get().run {
                val user = userRepository.getOne(userId)
                val category = categoryRepository.getCategory(categoryId, moimId)
                return if (category.isEmpty) {
                    Optional.empty()
                } else {
                    Optional.of(AccountItemDto(moimId, id, target, date, user, amount, Assets(moimId, assets), category.get()))
                }
            }

        }
    }

    fun getActiveItems(moimId: Long, date: String? = null): List<AccountItemDto> {
        val result = mutableListOf<AccountItemDto>()
        val itemList = if (date.isNullOrEmpty()) {
            accountItemRepository.getActiveAccountItem(moimId)
        } else {
            accountItemRepository.getActiveAccountItemFromDate(moimId, date)
        }
        itemList.forEach {
            val user = userRepository.getOne(it.userId)
            val category = categoryRepository.getCategory(it.categoryId, moimId)
            if (!category.isEmpty) {
                result.add(AccountItemDto(moimId, it.id, it.target, it.date, user, it.amount, Assets(moimId, it.assets), category.get(), it.content, it.memo))
            }
        }

        return result
    }

    fun getNonActiveItems(moimId: Long, date: String? = null): List<AccountItemDto> {
        val result = mutableListOf<AccountItemDto>()
        val itemList = if (date.isNullOrEmpty()) {
            accountItemRepository.getNonActiveAccountItem(moimId)
        } else {
            accountItemRepository.getNonActiveAccountItemFromDate(moimId, date)
        }
        itemList.forEach {
            val user = userRepository.getOne(it.userId)
            val category = categoryRepository.getCategory(it.categoryId, moimId)
            if (!category.isEmpty) {
                result.add(AccountItemDto(moimId, it.id, it.target, it.date, user, it.amount, Assets(moimId, it.assets), category.get(), it.content, it.memo))
            }
        }

        return result
    }

    fun deleteAccountItem(id: Long, moimId: Long) {
        accountItemRepository.removeAccountItem(id, moimId)
    }
}