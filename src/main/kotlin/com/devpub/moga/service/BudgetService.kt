package com.devpub.moga.service

import com.devpub.moga.api.model.entity.BudgetEntity
import com.devpub.moga.api.model.entity.BudgetSettingRequest
import com.devpub.moga.api.model.entity.MoimEntity
import com.devpub.moga.repository.BudgetRepository
import com.devpub.moga.repository.CategoryRepository
import com.devpub.moga.repository.MoimRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class BudgetService {

    @Autowired
    lateinit var budgetRepository: BudgetRepository

    @Autowired
    lateinit var categoryRepository: CategoryRepository

    fun saveBudget(budget: BudgetEntity): BudgetEntity {
        budgetRepository.activeBudget(budget.categoryId, budget.moimId)
        return budgetRepository.save(budget)
    }

    fun saveBudgetList(budgetSettingRequest: BudgetSettingRequest): List<BudgetEntity> {
        budgetSettingRequest.budgetList.firstOrNull()?.run {
            budgetRepository.activeBudgets(budgetSettingRequest.budgetList.map { it.categoryId }, moimId)
            budgetRepository.saveAll(budgetSettingRequest.budgetList)
            budgetRepository.removeBudgets(budgetSettingRequest.removeBudgetList.map { it.categoryId }, moimId)
            return budgetRepository.getBudgetList(moimId)
        }
        return listOf()
    }

}