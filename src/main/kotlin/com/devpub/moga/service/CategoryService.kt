package com.devpub.moga.service

import com.devpub.moga.api.model.entity.CategoryEntity
import com.devpub.moga.repository.CategoryRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import com.devpub.moga.api.model.Target;
import com.devpub.moga.repository.AccountItemRepository

@Service
class CategoryService {

    @Autowired
    lateinit var categoryRepository: CategoryRepository

    @Autowired
    lateinit var accountItemRepository: AccountItemRepository

    fun initCategories() {
        val categoryList: MutableList<CategoryEntity> = mutableListOf()
        categoryList.add(
            CategoryEntity(
                "전체",
                0,
                0,
                target = Target.EXPEND,
                moimId = null
            )
        )
        categoryList.createCategory(1, "식비/외식", "식비,외식,음식,중식,일식,한식,분식,양식")
        categoryList.createCategory(1, "마트/편의점", "마트,편의점,식자재,식품")
        categoryList.createCategory(2, "유흥/술", "유흥,술,주류")
        categoryList.createCategory(2, "담배")
        categoryList.createCategory(3, "패션/미용", "패션,미용,아울렛")
        categoryList.createCategory(3, "쇼핑", "쇼핑")
        categoryList.createCategory(4, "주거/관리비")
        categoryList.createCategory(5, "생활용품")
        categoryList.createCategory(6, "건강/의료", "건강,의료,병원,의원")
        categoryList.createCategory(6, "운동", "운동,스포츠,헬스,수영,테니스,탁구")
        categoryList.createCategory(7, "대중교통/차량", "대중교통,차량,지하철,버스,기차,주차장")
        categoryList.createCategory(7, "택시")
        categoryList.createCategory(8, "통신")
        categoryList.createCategory(9, "문화생활", "문화,예술,영화,연극,뮤지컬")
        categoryList.createCategory(10, "교육", "교육,학습,태권도")
        categoryList.createCategory(11, "숙박/여행", "숙박,여행,모텔,호텔,여관,관광,항공")
        categoryList.createCategory(12, "부모님")
        categoryList.createCategory(12, "경조사/회비")
        categoryList.createCategory(13, "기부")
        categoryList.createCategory(13, "세금/이자")
        categoryList.createCategory(12, "기타")

        categoryList.createCategory(1, "급여/월급", target = Target.INCOME)
        categoryList.createCategory(2, "용돈", target = Target.INCOME)
        categoryList.createCategory(3, "상여/성과금", target = Target.INCOME)
        categoryList.createCategory(4, "투자수익", target = Target.INCOME)
        categoryList.createCategory(5, "더치페이", target = Target.INCOME)
        categoryList.createCategory(6, "기타", target = Target.INCOME)

        categoryRepository.saveAll(categoryList)
    }

    fun saveCategory(categoryEntity: CategoryEntity): CategoryEntity {
        categoryEntity.moimId?.run {
            categoryRepository.activeCategory(categoryEntity.id, this)
        }
        return categoryRepository.save(categoryEntity)
    }

    fun saveCategoryList(categoryEntityList: List<CategoryEntity>): List<CategoryEntity> {
        return categoryRepository.saveAll(categoryEntityList)
    }

    fun removeCategory(categoryId: Long, moimId: Long) {
        val removeCategory = categoryRepository.getCategory(categoryId, moimId)
        if (!removeCategory.isEmpty) {
            removeCategory.get().run {
                accountItemRepository.updateAccountItemCategory(moimId, categoryId, parentId)
            }
        }
        categoryRepository.removeCategory(categoryId, moimId)
    }

    private fun MutableList<CategoryEntity>.createCategory(
        kosisId: Long,
        name: String,
        tag: String = name.replace("/", ","),
        target: Target = Target.EXPEND,
    ) {
        add(
            CategoryEntity(
                name,
                this@createCategory.size.toLong(),
                kosisId,
                target,
                tag,
                null
            )
        )
    }
}