package com.devpub.moga.service

import com.devpub.moga.api.model.dto.BudgetDto
import com.devpub.moga.api.model.dto.MoimDto
import com.devpub.moga.api.model.dto.MoimRequest
import com.devpub.moga.api.model.entity.BudgetEntity
import com.devpub.moga.api.model.entity.MoimEntity
import com.devpub.moga.repository.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.RequestParam
import java.lang.StringBuilder
import java.util.*

@Service
class MoimService {

    @Autowired
    lateinit var moimRepository: MoimRepository

    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var categoryRepository: CategoryRepository

    @Autowired
    lateinit var budgetRepository: BudgetRepository

    @Autowired
    lateinit var accountItemRepository: AccountItemRepository

    fun saveMoim(moimRequest: MoimRequest): MoimEntity {

        val sb = StringBuilder()
        moimRequest.userList.forEachIndexed { index, userEntity ->
            userRepository.save(userEntity)
            sb.append(userEntity.id)
            if (index > 0) {
                sb.append(",")
            }
        }
        val moimEntity = MoimEntity(moimRequest.id, moimRequest.ownerId, sb.toString())
        moimRepository.activeMoim(moimRequest.id)
        moimRepository.save(moimEntity)
        moimRequest.budgetList.forEach {
            budgetRepository.activeBudget(it.categoryId, it.moimId)
            budgetRepository.save(it)
        }

        return moimRepository.save(moimEntity)
    }

    fun updateMoim(moimEntity: MoimEntity): MoimEntity {
        return moimRepository.save(moimEntity)
    }

    fun getMoimEntity(id: Long): Optional<MoimEntity> {
        return moimRepository.findById(id)
    }

    fun getMoim(id: Long): Optional<MoimDto> {
        val moimEntity = moimRepository.findById(id)
        if (!moimEntity.isEmpty) {
            val categoryList = categoryRepository.getCategoryList(id)
            val budgetEntityList = budgetRepository.getBudgetList(id)
            val budgetList = mutableListOf<BudgetDto>()
            budgetEntityList.forEach {
                val category = categoryRepository.getCategory(it.categoryId, it.moimId)
                if (!category.isEmpty) {
                    budgetList.add(
                        BudgetDto(
                            it.moimId,
                            category.get().copy(moimId = it.moimId),
                            it.budget,
                            it.weeklyBudget,
                            it.maxBudget
                        )
                    )
                }
            }

            return Optional.of(MoimDto().apply {
                moimEntity.get().let {
                    this.id = it.id
                    this.ownerId = it.ownerId
                    this.createdDate = it.createdDate
                    this.updatedDate = it.updatedDate
                    val userList = userRepository.getUserList(it.userList)
                    this.userList.addAll(userList)
                }
                this.categoryList.addAll(categoryList)
                this.budgetList.addAll(budgetList)
            })
        }

        return Optional.empty<MoimDto>()
    }

    fun getUpdateMoimData(id: Long, date: String?) {

    }

    fun removeMoim(moimEntity: MoimEntity) {
        moimEntity.apply {
            budgetRepository.removeBudgetAll(id)
            categoryRepository.removeCategoryAll(id)
            accountItemRepository.removeAccountItemAll(id)
            moimRepository.removeMoim(id)
        }
    }
}