package com.devpub.moga.service

import com.devpub.moga.api.model.dto.PayMessage
import org.springframework.stereotype.Service
import java.util.*
import java.util.regex.Pattern
import com.devpub.moga.api.model.Target
import com.devpub.moga.api.model.dto.NotiRequest
import com.devpub.moga.api.model.entity.CategoryEntity
import com.devpub.moga.helper.KakaoRestApiHelper
import com.devpub.moga.repository.CategoryRepository
import com.devpub.moga.util.DateUtil
import com.devpub.moga.util.MogaUtil
import org.springframework.beans.factory.annotation.Autowired
import kotlin.Exception

@Service
class ParseService {
    @Autowired
    lateinit var categoryRepository: CategoryRepository

    @Autowired
    lateinit var kakaoRestApiHelper: KakaoRestApiHelper

    fun parseSmsMessage(moimid: Long, text: String, date: Date = DateUtil.todayDate): PayMessage? {
        return parseContentMessage(text, date)?.let { payMessage ->
            if (payMessage.target == null || payMessage.payMoney <= 0 || payMessage.assetsName.isEmpty()) {
                return null
            }

            try {
                payMessage.time = DateUtil.yyyy_MM_dd_HH_mm.parse(getDateTime(payMessage.content))
            } catch (e: Exception) {
            }
            payMessage.category = getCategory(moimid, payMessage)
            return payMessage
        }
    }

    fun parseNotification(notiRequest: NotiRequest): PayMessage? {
        val moimid = notiRequest.moimId
        val packageName = notiRequest.packagerName
        val title = notiRequest.title
        val text = notiRequest.text
        val date = notiRequest.date

        val isCard = isCard(title) || isCard(text)
        val isBank = isBank(title) || isBank(text)
        if (isCard || isBank) {
            val payMessage = PayMessage(date)
            val content = getExceptedTotalMoneyText(text)
            payMessage.content = content.trim()
            payMessage.payMoney = getMoney(content)
            payMessage.target = getTransaction(content)
            payMessage.isBank = isBank(title) || isBank(content)

            when {
                packageName.contains("com.shinhan") -> {
                    /**
                    packageName: com.shinhan.sbanking
                    title: 입금
                    text : 1원 입출금(6524) 06.27 15:04
                    정효찬 잔액 1,075,380원
                    subText: 입출금 알림
                     */
                    payMessage.assetsName = "신한은행"
                    payMessage.target = if (title.contains("입금")) {
                        Target.INCOME
                    } else {
                        Target.TRANSFER
                    }
                }
                packageName.contains("com.kakaobank") -> {
                    /**
                    packageName: com.kakaobank.channel
                    title: 입출금내역 안내
                    text : 정*찬(4516) 06/27 15:04 출금 1원 정효찬 잔액 963,061원
                    subText: null
                     */
                    payMessage.assetsName = "카카오뱅크"
                }
                packageName.contains("com.kbankwith") -> {
                    /**
                    packageName: com.kbankwith.smartbank
                    title: 케이뱅크
                    text : 출금 1원
                    정효찬 | 듀얼K입출금통장(0088)
                    잔액 765,756원
                    subText: null
                     */
                    payMessage.assetsName = "케이뱅크"
                    val storeMatcher = Pattern.compile("\\S+\\s\\|").matcher(content)
                    if (storeMatcher.find()) {
                        val storeName = storeMatcher.group()
                        payMessage.storeName = storeName.substring(0, storeName.length - 1).trim()
                    }
                }
                packageName.contains("viva.republica") -> {
                    /**
                    packageName: viva.republica.toss
                    title: 1원 출금
                    text : 내 토스증권 계좌 → 정효찬님
                    subText: null
                     */
                    payMessage.assetsName = "토스"
                    payMessage.payMoney = getMoney(title)
                    payMessage.target = getTransaction(title)
                }
                packageName.contains("com.IBK") -> {
                    /**
                    packageName: com.IBK.SmartPush.app
                    title: 입출금내역 알림
                    text : [입금] 1원 정효찬
                    021-******-01-015
                    06/27 17:21
                    subText: null
                     */
                    payMessage.assetsName = "기업은행"
                }
                packageName.contains("com.kbstar") -> {
                    /**
                    packageName: com.kbstar.starpush
                    title: KB스타알림
                    text : 06/27 17:21 758602-**-***648 기업정효찬 스마트폰출금 1 잔액781,716
                    subText: null
                     */
                    payMessage.assetsName = "KB국민은행"
                    val moneyMatcher = Pattern.compile("(\\s[\\d,]+\\s)").matcher(content)
                    if (moneyMatcher.find()) {
                        payMessage.payMoney =
                            getRegexString(moneyMatcher.group(), "\\d").toLong()
                    }

                    payMessage.storeName = getStoreName(
                        false,
                        payMessage.assetsName,
                        content,
                        "|([\\d,]+)"
                    )
                }
                else -> {
                    if (text.length <= 160) {
                        parseContentMessage(text, date)?.apply {
                            payMessage.assetsName = assetsName
                            payMessage.content = content
                            payMessage.isBank = isBank
                            payMessage.payMoney = payMoney
                            payMessage.storeName = storeName
                            payMessage.target = target
                        }
                    } else {
                        payMessage.payMoney = 0L
                    }
                }
            }
            if (payMessage.assetsName.isNotEmpty() && payMessage.storeName.isEmpty()) {
                payMessage.storeName =
                    getStoreName(false, payMessage.assetsName, content)
            }

            if (payMessage.target == null || payMessage.payMoney <= 0 || payMessage.assetsName.isEmpty()) {
                return null
            }

            try {
                payMessage.time = DateUtil.yyyy_MM_dd_HH_mm.parse(getDateTime(payMessage.content))
            } catch (e: Exception) {
            }
            payMessage.category = getCategory(moimid, payMessage)

            return payMessage
        }
        return null
    }

    private fun parseContentMessage(text: String, date: Date = DateUtil.todayDate): PayMessage? {
        try {
            val numberAndLetter = "[^\\[\\]\\(\\)\\-]"

            val isCard = isCard(text)
            val isBank = isBank(text)
            if (isCard || isBank) {
                val payMessage = PayMessage(date)
                val content = getExceptedTotalMoneyText(text)
                payMessage.content = content.trim()
                payMessage.isBank = isBank

                if (isCard) {
                    var cardText: String
                    val cardStatusText: String
                    val cardMatcher = CARD_PATTERN.matcher(content)
                    cardMatcher.find()
                    if (cardMatcher.groupCount() > 1) {
                        cardText = cardMatcher.group(cardMatcher.groupCount() - 1)
                        val subCardMatcher = CARD_PATTERN.matcher(cardText)
                        while (subCardMatcher.find()) {
                            if (subCardMatcher.groupCount() > 1) {
                                cardText = subCardMatcher.group(subCardMatcher.groupCount() - 1)
                            }
                        }
                        cardStatusText = cardMatcher.group(cardMatcher.groupCount())
                    } else {
                        val cardMatchingText = cardMatcher.group()
                        val cardDetailMatcher = Pattern.compile("승인|취소").matcher(cardMatchingText)
                        if (cardDetailMatcher.find()) {
                            cardText = cardMatchingText.substring(0, cardDetailMatcher.start())
                            cardStatusText = cardDetailMatcher.group()
                        } else {
                            cardText = cardMatchingText
                            cardStatusText = "승인"
                        }
                    }
                    payMessage.assetsName = getRegexString(cardText.replace("Web발신", ""), numberAndLetter).trim()
                    if (cardStatusText.contains("취소")) {
                        payMessage.target = Target.INCOME
                    } else {
                        payMessage.target = Target.EXPEND
                    }
                } else {
                    val bankNameMatcher =
                        Pattern.compile("(?:.*은행|.*뱅크|(.*기업|농협|우리|신한|NH|씨티|카카오|케이|국민|KB|하나|부산|수협|전북|제주|광주|경남))")
                            .matcher(content)

                    if (bankNameMatcher.find()) {
                        val bankNameText = bankNameMatcher.group()
                        payMessage.assetsName = getRegexString(bankNameText.replace("Web발신", ""), numberAndLetter).trim()
                    }

                    payMessage.target = getTransaction(content)
                }

                payMessage.payMoney = getMoney(content)
                payMessage.storeName = getStoreName(isCard, payMessage.assetsName, content)

                return payMessage
            }
        } catch (e: Exception) {
        }
        return null
    }

    private fun isCard(text: String): Boolean {
        val cardMatcher = CARD_PATTERN.matcher(text)
        return cardMatcher.find()
    }

    private fun isBank(text: String): Boolean {
        val bankMatcher = BANK_PATTERN.matcher(text)
        return bankMatcher.find()
    }

    private fun getExceptedTotalMoneyText(text: String): String {
        val contentText = text.replace("[Web발신]", "")
        val totalMoneyMatcher = TOTAL_MONEY_PATTERN.matcher(contentText)
        if (totalMoneyMatcher.find()) {
            return contentText.replace(totalMoneyMatcher.group(), "")
        }
        return contentText
    }

    private fun getTransaction(content: String): Target? {
        val transactionMatcher = TRANSACTION_PATTERN.matcher(content)
        if (transactionMatcher.find()) {
            val transactionText = transactionMatcher.group()
            return when {
                transactionText.contains("지급") -> {
                    Target.TRANSFER
                }
                transactionText.contains("출금") -> {
                    Target.TRANSFER
                }
                else -> {
                    Target.INCOME
                }
            }
        }

        return null
    }

    private fun getMoney(content: String): Long {
        val moneyMatcher = MONEY_PATTERN.matcher(content)
        if (moneyMatcher.find()) {
            return try {
                getRegexString(moneyMatcher.group(), "\\d").toLong()
            } catch (e: Exception) {
                0
            }
        }
        return 0
    }

    private fun getStoreName(
        isCard: Boolean,
        assetsName: String,
        content: String,
        additionalRegex: String = "",
    ): String {
        var storeName = ""
        try {
            val fitStoreMatcher = Pattern.compile(".*가맹점명:(.+)").matcher(content)
            if (fitStoreMatcher.find()) {
                return fitStoreMatcher.takeIf { it.groupCount() > 0 }?.group(1)?.trim() ?: ""
            }
        } catch (e:Exception) {
            storeName = ""
        }

        var startMerge = false
        val storeMatcher = Pattern.compile("\\s*\\S*\\s*").matcher(content)
        while (storeMatcher.find()) {
            val storeTempText = storeMatcher.group()
            if (storeTempText.trim().isNotEmpty()) {
                if (isCard) {
                    val totalTextMatcher =
                        Pattern.compile("누적|잔여|잔액|승인|([\\d|,]+원)").matcher(storeTempText)
                    if (!totalTextMatcher.find() && startMerge) {
                        storeName += storeTempText
                    }
                    if (!startMerge) {
                        val startTextMatcher = TIME_PATTERN.matcher(storeTempText)
                        startMerge = startTextMatcher.find()
                    }
                } else {
                    val ignoreMatcher =
                        Pattern.compile("누적|잔여|잔액|입금|출금|지급|승인|(.*Web.*)|([\\d|,]+원)|(\\d{2}[/.:]\\d{2})|(.*(?=[*]).*)$additionalRegex")
                            .matcher(storeTempText)
                    if (!ignoreMatcher.find() && !storeTempText.contains(assetsName)) {
                        storeName += storeTempText
                    }
                }
            }
        }
        return storeName.trim()
    }

    private fun getDateTime(content: String): String {
        var dateTime = DateUtil.getYearText()
        val dateMatcher = DAY_PATTERN.matcher(content)
        val timeMatcher = TIME_PATTERN.matcher(content)
        if (dateMatcher.find()) {
            dateTime += "/${dateMatcher.group()}"
        }

        if (timeMatcher.find()) {
            dateTime += " ${timeMatcher.group()}"
        }
        return dateTime
    }

    private fun getCategory(moimId: Long, payMessage: PayMessage): CategoryEntity? {
        val etcCategoryName = "기타"

        val categoryName = try {
            kakaoRestApiHelper.getSearchPlaceFirstCategory(payMessage.storeName) ?: etcCategoryName
        } catch (e: Exception) {
            etcCategoryName
        }

        var etcCategory: CategoryEntity? = null
        return categoryRepository.getCategoryList(moimId).filter { MogaUtil.isSameTarget(it.target, payMessage.target) }
            .find { category ->
                if (category.name == etcCategoryName) {
                    etcCategory = category
                }

                !category.tag.split("/|,".toRegex()).findLast { subCategory ->
                    categoryName.contains(subCategory)
                }.isNullOrEmpty()
            } ?: etcCategory
    }

    private fun getRegexString(str: String, regex: String): String {
        val sb = StringBuffer()
        if (str.isNotEmpty()) {
            val pattern = Pattern.compile(regex)
            val matcher = pattern.matcher(str)
            while (matcher.find()) {
                sb.append(matcher.group())
            }
        }
        return sb.toString()
    }

    companion object {
        private val CARD_PATTERN = Pattern.compile("(.+)(승인|취소|체크|신용)")
        private val BANK_PATTERN = Pattern.compile("출금|지급|입금|입출금")
        private val TRANSACTION_PATTERN = Pattern.compile("지급|입금|출금")
        private val MONEY_PATTERN = Pattern.compile("([\\d,]+원)")
        private val TOTAL_MONEY_PATTERN = Pattern.compile("(?:누적|잔여|잔액)\\s?(?:[\\d,]+원?)")
        private val DAY_PATTERN = Pattern.compile("\\d{2}[/.]\\d{2}\\s")
        private val TIME_PATTERN = Pattern.compile("\\d{2}:\\d{2}")
    }
}