package com.devpub.moga.service

import com.devpub.moga.model.CommonResponse
import com.devpub.moga.model.ApiDataResponse
import org.springframework.stereotype.Service

@Service // 해당 Class가 Service임을 명시합니다.
class ResponseService {

    // enum으로 api 요청 결과에 대한 code, message를 정의합니다.
    enum class ResponseCode(var code: Int, var message: String) {
        SUCCESS(0, "성공하였습니다."),
        FAIL(-1, "실패하였습니다.")
    }

    // 단일건 결과를 처리하는 메소드
    fun <T> result(data: T) = ApiDataResponse<T>().apply {
        this.data = data
        this.successResult()
    }

    fun resultFail(code: Int = 0, message: String) = CommonResponse().apply {
        this.success = false
        this.code = code
        this.message = message
    }

    // 결과 모델에 api 요청 성공 데이터를 세팅해주는 메소드
    // 성공 결과만 처리하는 메소드
    val successResult: CommonResponse
        get() = CommonResponse().successResult()

    // 실패 결과만 처리하는 메소드
    val failResult: CommonResponse
        get() = CommonResponse().failResult()

    private fun CommonResponse.failResult(): CommonResponse {
        return this.apply {
            success = false
            code = ResponseCode.FAIL.code
            message = ResponseCode.FAIL.message
        }
    }

    private fun CommonResponse.successResult(): CommonResponse {
        return this.apply {
            success = true
            code = ResponseCode.SUCCESS.code
            message = ResponseCode.SUCCESS.message
        }
    }
}