package com.devpub.moga.service

import com.devpub.moga.api.model.entity.SuggestEntity
import com.devpub.moga.repository.SuggestRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class SuggestService {

    @Autowired
    lateinit var suggestRepository: SuggestRepository

    fun saveSuggest(suggestEntity: SuggestEntity): SuggestEntity {
        return suggestRepository.save(suggestEntity)
    }
}