package com.devpub.moga.service

import com.devpub.moga.api.model.entity.UserEntity
import com.devpub.moga.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class UserService {

    @Autowired
    lateinit var userRepository: UserRepository

    fun saveUser(userEntity: UserEntity): UserEntity {
        return userRepository.save(userEntity)
    }

    fun updateUser(userEntity: UserEntity): UserEntity {
        return userRepository.save(userEntity)
    }

    fun getUser(id: Long): Optional<UserEntity> {
        return userRepository.findById(id)
    }

    fun getUserForDelete(id: Long): Optional<UserEntity> {
        return userRepository.findByIdForNonActive(id)
    }
}