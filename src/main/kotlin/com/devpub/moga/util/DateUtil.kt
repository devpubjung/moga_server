package com.devpub.moga.util

import java.text.DateFormat
import java.text.DateFormatSymbols
import java.text.SimpleDateFormat
import java.util.*

object DateUtil {

    val yyyy = SimpleDateFormat("yyyy")
    val MM = SimpleDateFormat("MM")
    val mm = SimpleDateFormat("mm")
    val dd = SimpleDateFormat("dd")
    val yyyyMM = SimpleDateFormat("yyyyMM")
    val yyyy_MM = SimpleDateFormat("yyyy.MM")
    val yyyyMMdd = SimpleDateFormat("yyyyMMdd")
    val yyyy_MM_dd = SimpleDateFormat("yyyy.MM.dd")
    val MM_dd = SimpleDateFormat("MM/dd")
    val yyyy_MM_dd_HH_mm = SimpleDateFormat("yyyy/MM/dd HH:mm")
    val yyyy_MM_dd_HH_mm_ss = SimpleDateFormat("yyyy/MM/dd HH:mm:ss")
    val serverDateFormat : DateFormat = SimpleDateFormat("yyyy/MM/dd HH:mm:ss")

    val todayDate: Date get() = today.time

    val today: Calendar
        get() {
            val cal = Calendar.getInstance(Locale.KOREAN)
            cal.time = Date()
            return cal
        }

    fun thisYear(): Int {
        return Integer.parseInt(yyyy.format(Date(System.currentTimeMillis())))
    }

    fun thisMonth(): Int {
        return Integer.parseInt(MM.format(Date(System.currentTimeMillis())))
    }

    fun equalsMonth(target1: Date, target2: Date): Boolean {
        return yyyyMM.format(target1) == yyyyMM.format(target2)
    }

    fun getYearText(calendar: Calendar = today): String {
        return yyyy.format(calendar.time)
    }

    fun getMonthText(calendar: Calendar = today): String {
        return MM.format(calendar.time)
    }

    fun getFullMonthText(calendar: Calendar = today): String {
        return "${MM.format(calendar.time).toInt()}월"
    }

    fun getBirthday(date: Date): String {
        return yyyy_MM_dd.format(date)
    }

    fun getServerDateString(date: Date) :String{
        return serverDateFormat.format(date)
    }

    fun containsThisWeek(date: Date): Boolean {
        return containsTargetWeek(date)
    }

    fun containsTargetWeek(curDate: Date, targetDate: Date = todayDate): Boolean {
        val timeGap = 1000
        val firstDate = getFirstDayOfWeek(targetDate).time / timeGap
        val lastDate = getLastDayOfWeek(targetDate).time / timeGap
        return (curDate.time / timeGap) in firstDate..lastDate
    }

    fun getWeekPeriod(date: Date = todayDate): String {
        val firstDate = getFirstDayOfWeek(date)
        val lastDate = getLastDayOfWeek(date)
        return "${MM_dd.format(firstDate)} ~ ${MM_dd.format(lastDate)}"
    }

    fun getFirstDayOfWeek(date: Date): Date {
        val cal = Calendar.getInstance(Locale.KOREA)
        cal.time = date
        if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            cal.add(Calendar.DATE, -7)
        }
        cal.add(Calendar.DATE, Calendar.SUNDAY + 1 - cal.get(Calendar.DAY_OF_WEEK))
        cal.set(Calendar.HOUR, -12)
        cal.set(Calendar.MINUTE, 0)
        cal.set(Calendar.SECOND, 0)

        return cal.time
    }

    fun getLastDayOfWeek(date: Date): Date {
        val cal = Calendar.getInstance(Locale.KOREA)
        cal.time = date
        if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            cal.add(Calendar.DATE, -7)
        }
        cal.add(Calendar.DATE, Calendar.SATURDAY + 1 - cal.get(Calendar.DAY_OF_WEEK))
        cal.set(Calendar.HOUR, 11)
        cal.set(Calendar.MINUTE, 59)
        cal.set(Calendar.SECOND, 59)

        return cal.time
    }

    fun getDateString(date: Date = today.time): String {
        val dateString = SimpleDateFormat("yyyy/MM/dd").format(date)
        return "$dateString (${getDayOfWeek(date)})"
    }

    fun getTimeString(date: Date = today.time): String {
        val cal = Calendar.getInstance(Locale.KOREA)
        cal.time = date
        val dateString = SimpleDateFormat("hh:mm").let {
            it.timeZone = cal.timeZone
            it.format(date)
        }
        val afternoon = when (cal.get(Calendar.AM_PM)) {
            Calendar.AM -> "오전"
            Calendar.PM -> "오후"
            else -> ""
        }
        return "$afternoon $dateString"
    }

    fun getDayOfWeek(date: Date = today.time): String {
        val cal = Calendar.getInstance()
        cal.time = date
        return when (cal[Calendar.DAY_OF_WEEK]) {
            Calendar.SUNDAY -> "일"
            Calendar.MONDAY -> "월"
            Calendar.TUESDAY -> "화"
            Calendar.WEDNESDAY -> "수"
            Calendar.THURSDAY -> "목"
            Calendar.FRIDAY -> "금"
            Calendar.SATURDAY -> "토"
            else -> ""
        }
    }

    fun getMonthSymbol(): Array<String> = DateFormatSymbols(Locale.KOREA).months

    fun getShortMonthSymbol(): Array<String> = DateFormatSymbols(Locale.KOREA).shortMonths

    fun getShortWeekSymbol(): Array<String?> {
        val weeks = DateFormatSymbols(Locale.KOREA).shortWeekdays.filterNot { it.isEmpty() }
        return getWeekTexts(weeks)
    }

    fun getWeekSymbol(): Array<String?> {
        val weeks = DateFormatSymbols(Locale.KOREA).weekdays.filterNot { it.isEmpty() }
        return getWeekTexts(weeks)
    }

    private fun getWeekTexts(weeks: List<String>): Array<String?> {
        val weekTexts = arrayOfNulls<String>(7)
        for (i in weekTexts.indices) {
            val dayIndex: Int = i % weeks.size
            weekTexts[i] = weeks[(dayIndex + 1) % 7]
        }

        return weekTexts
    }

    fun sameDate(date1: Date?, date2: Date?): Boolean {
        if (date1 == null || date2 == null) {
            return false
        }
        return yyyyMMdd.format(date1) == yyyyMMdd.format(date2)
    }

}